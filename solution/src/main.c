#include "../header/bmp_utils.h"
#include "../header/files.h"
#include "../header/image.h"
#include "../header/rotate.h"
#include <stdio.h>

int main(int argc, char** argv ) {
	if (argc != 3) {
		fprintf(stderr, "Incorrect parametrs");
		return 1;
	}
	
	FILE* in = NULL;
	FILE* out = NULL;
	
	if (open_file(&in, argv[1], "rb") == false) {
		fprintf(stderr, "Can't open file to read");
		return 1;
	}
	
	if (open_file(&out, argv[2], "wb") == false) {
		fprintf(stderr, "Can't open file to write");
		close_file(&in);
		return 1;
	}
	
	printf("Opened files");
	
	
	struct image img = {0};
	if (from_bmp(in, &img) != READ_OK) {
		fprintf(stderr, "Can't read file");
		destroy_image(&img);
		close_file(&in);
		close_file(&out);
		return 1;
	}
	
	printf("From bmp readed");
	
	
	struct image rotated = rotate(&img);
	if (rotated.width != 0 && to_bmp(out, &rotated) != WRITE_OK) {
		fprintf(stderr, "Can't write file");
		destroy_image(&rotated);
		close_file(&in);
		close_file(&out);
		return 1;
	}
	
	printf("Image rotated");
	
	bool close_in = !close_file(&in);
	bool close_out = !close_file(&out);
	
	if (close_in || close_out) {
		fprintf(stderr, "Can't close file");
		destroy_image(&img);
		destroy_image(&rotated);
		return 1;
	}
	destroy_image(&img);
	destroy_image(&rotated);	
    return 0;
}
