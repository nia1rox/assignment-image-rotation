#include "../header/rotate.h"

struct image rotate(const struct image *src ) {
    struct image copy_image = create_image(src->height,src->width);
    if (copy_image.pixels == NULL) {
    	return (struct image){0};
    }
    for (size_t i = 0; i < src->height; i++) {
        for (size_t j = 0; j < src->width; j++) {
          copy_image.pixels[src->height*j + src->height-i-1] = src->pixels[src->width*i + j];
        }
    }
    return copy_image;
}
