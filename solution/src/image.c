#include "../header/image.h"

#include <malloc.h>

void image_destroy(const struct image *img) {
	free(img->pixels);
}

static struct pixel * pix_size(size_t width, size_t height) {
	struct pixel *pixels = malloc(width * height * sizeof(struct pixel));
	return pixels;
}

struct image create_image(size_t width, size_t height) {
	struct image img = {0};
	img.width = width;
	img.height = height;
	img.pixels = pix_size(width, height);
	return img;
}

void destroy_image(const struct image *img) {
	free(img->pixels);
}

